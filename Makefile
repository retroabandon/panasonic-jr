DISASSEMBLIES := $(wildcard */*.dis)

all: $(DISASSEMBLIES)
.PHONY: all

MAKEFLAGS += --no-builtin-rules		# Makes -d output _much_ shorter.

%.dis: %.bin %.info common.info f9dasm/f9dasm
	f9dasm/f9dasm -cchar ';' -out $@ -info $*.info $<
	bin/f9post $@

#   f9dasm currently has no build system, but building it ourselves here
#   might be easier than using one provided by that repo anyway.
#   `ignore = untracked` in ../.gitmodules avoids Git seeing the submodule
#   as modified because we build `f9dasm` in the source directory.
f9dasm/f9dasm: f9dasm/f9dasm.c
	cc -o f9dasm/f9dasm f9dasm/f9dasm.c

#   Check out the submodule if that's not already been done.
f9dasm/f9dasm.c:
	git submodule update --init f9dasm

