include common.info

*   Conventions:
*   - _nnnn are local labels, not used outside of that routine.
*     Other names may be used when too many _nnnn would be confusing.
*   - _rts_nnnn are "local" RTS locations

comment        1   .   Basic Program Text Format:
comment        1
comment        1   .   BASIC program text starts at `btext`, $0801 by default.
comment        1   .   ($800 is set to $00; ??? we do not yet know why.)
comment        1
comment        1   .   Each line of code is:
comment        1   .   ∙ 0,1  pointer to next line, or $0000 for end of text
comment        1   .   ∙ 2,3  line number
comment        1   .   ∙ 4... tokenized line terminated with $00
comment        1   .   The lines appear always to be in strictly ascending order in memory.
comment        1

comment       43
label         43    m0043
label         44    m0043+1

comment       45
comment       45   .   ??? Pointer to current BASIC line being run?
comment       45   .   The BIOS also knows the first byte of this as pr_suppress,
comment       45   .   which when non-0 suppresses printing for `prstr*q` routines.
comment       45   .   However, this BASIC does not use this.
label         45    progline
label         46    progline+1
label         47    findline_target
lcomment      47    param for findline
label         48    findline_target+1

comment       47

comment       A3

comment     004F
comment     004F   .   BASIC memory management pointers
label       004F    bptr4F
lcomment    004F    ??? init sets to start of area after program text
label       0050    bptr4F+1
label       0051    bptr51
lcomment    0051    ??? init sets to start of area after program text
label       0052    bptr51+1
label       0053    bptr53
lcomment    0053    ??? init sets to start of area after program text
label       0054    bptr53+1
label       0055    bptr55
lcomment    0055    ??? init sets to start of area after program text
label       0056    bptr55+1

comment     0069
label       0069    btext
lcomment    0069    start of BASIC program text
label       006A    btext+1
label       006F    bptr6F
lcomment    006F    ??? init sets to start of area after program text
label       0070    bptr6F+1
label       0071    bptr71
label       0072    bptr71+1
lcomment    0071    ??? init sets to start of area after program text

comment     00AB
label       00AB    stacksave0
lcomment    00AB    ??? saved stack pointer
label       00AD    stacksave1
lcomment    00AD    ??? saved stack pointer
label       00AE    stacksave1+1

comment     00E0

comment     0112
label       0112    m0112
label       0113    m0112+1

comment     0118
comment     0118   .   BASIC keyword extensions vector. If not $0000, this is called when
comment     0118   .   the internal keyword parser fails to match, allowing an extension
comment     0118   .   to add additional keywords.
label       0118    vec_kwext

comment     011A
label       011A    jmp_011a
label       011D    jmp_011d

comment     0120

comment     0124
label       0124    jmp_0124
label       0127    jmp_0127

comment     012A
comment     012A   .   Error messages $01 through $13 are handled by perror.
comment     012A   .   If perror receives a code >$13 in B it assumes that an expansion
comment     012A   .   system has set up jmp_perrorext to JMP to the routine to handle
comment     012A   .   codes $14 and above, and jumps to it. That routine can probably
comment     012A   .   just load X with the address of its error message (terminated by
comment     012A   .   a final letter with the high bit set) and JMP to _per_printmsg
comment     012A   .   to print it followed by " error" and optionally the program line
comment     012A   .   number and continue in the standard way.
label       012A    jmp_perrorext
comment     012D
label       012D    jmp_ejmp
lcomment    012D    init to JMP instruction (for calling following vector)
label       012E    vec_ejmp
lcomment    012E    init to ejmp ($E000 in BIOS ROM)

label       0136    devtab
lcomment    0136    coldstart loads this to devtab_default
label       0138    vec_0138
lcomment    0138    init loads with $BD10

option offset A000

lcomment    A000    ??? 1 before start of basic text
const       A001
lcomment    A003    $800: reserved/clear byte (why???)
lcomment    A003    $801-802: init program text to empty program
lcomment    A005    set btext to this empty program at $801
lcomment    A008    sets bptr71
lcomment    A00E    ??? sound data location?
lcomment    A01C    JMP instruction
comment     A02D
comment     A033   .              ;   calculate and print free memory
lcomment    A03C    X-AB was left in tmp0

comment     A044
label       A044    clr_x3
lcomment    A044    clear 3 bytes pointed to by X
comment     A04B
comment     A05D
const       A063

comment     A068
label       A068    _a068
label       A06B    warmstart
label       A070    ready
lcomment    A070    ??? white
lcomment    A081    get non-zero value for flag
lcomment    A082    enable NMI handler
const       A085
label       A091    _a091

comment     A0A4   .              ;
label       A0A4    _a0a4
label       A0A9    _a0a9
label       A0B0    _a0b0
label       A0C2    _a0c2
label       A0C8    _a0c8
label       A0CD    _a0cd
label       A0D1    _a0d1
label       A0D4    _a0d4
label       A0E8    _a0e8
label       A0EB    _a0eb
label       A0F6    _a0f6
label       A0FC    _a0fc
comment     A108   .              ; fallthrough

comment     A108

comment     A12B
label       A12B    _a12b

comment     A173
lcomment    A173    bmsg_RedimArray index

comment     A178

comment     A1C4
comment     A1CE
comment     A1D8
comment     A1E6

comment     A2DD
lcomment    A2EB    call incx3 and return (TCO)
lcomment    A2ED    continue after JMP
label       A2EF    jmp_incx3
comment     A2F2

comment     A3CD
label       A3CF    _a3cf
label       A3D5    _a3d5
label       A3E7    _a3e7
label       A3EF    _a3ef

comment     A3F4

comment     A404
label       A422    _a422
label       A431    _a431
label       A438    _a438

comment     A51D
cvector     A51D-A550

comment     A551
lcomment    A554    stacksave1 moved up by 6 bytes
lcomment    A55E    top byte of previous stack
lcomment    A560    move X to 2nd byte of previous stack
label       A569    _rts_a569
comment     A56A   .              ;
label       A56a    _a56a
comment     A572   .              ;
label       A572    _sc_cv_rts_a572
label       A573    _clv_rts_a573

comment     A575

comment     A687

const       A69F

label       A73A    _a73a
label       A73E    _a73e
label       A76F    _a76f
label       A788    _a788
label       A7A1    _rts_a7a1

comment     A810
data        A810-A84D
word        A810-A82D

comment     A84E
comment     A84E   .   X and qnumptr = X+A  ♠X,qnumptr ♣A
label       A84E    addXqnp_A

comment     A85D

label       A8A8    usestack0
lcomment    A8A8    STS stacksave1, LDS stacksave0

comment     A8AD
lcomment    A8AD    space?
lcomment    A8AF    no, go ???
char        A8AE
lcomment    A8B1    consume space char
lcomment    A8B2    load next char
lcomment    A8B4    loop
comment     A8B6   .              ;
label       A8B6    _a8B6

label       A8EE    _a8ee

comment     A906
comment     A906   .   ??? Data? Or calculated jump?

lcomment    AA27   RTS
comment     AA2A   .              ;
label       AA32    _undefln_aa32

comment     AA35

comment     AA4A
comment     AA4F

label       AA72    _aa72

comment     AA75

comment     AAF7
lcomment    AB03    RTS

comment     AB06

const       AB11

label       AB7E    _rts_ab7e

comment     AB7F
label       AB8E    _ab8e
label       AB93    _ab93
label       AB96    _ab96
label       ABA0    _aba0
label       ABA5    _aba5
lcomment    ABA5    RTS
comment     ABA8   .              ;
label       ABA8    _aba8
comment     ABBD   .              ;
label       ABBD    _abbd
label       ABBF    _loop_abbf
comment     ABC5   .              ;
label       ABC5    _abc5
label       ABCB    _abcb
label       ABCF    _abcf
label       ABD6    _abd6
label       ABD8    _abd8
label       ABE2    _abe2
label       ABF6    _abf6
label       ABFC    _abfc
label       ABFE    _abfe
label       AC02    _ac02
label       AC12    _ac12
comment     AC1A   .              ;
label       AC1A    _fmtdec_ac1a

label       AC06    _ac06

comment     AC20

comment     AD27
comment     AD30

lcomment    AD83    bmsg_CONT index
label       AD85    _ad85
label       AD88    _ad88
lcomment    AD88    bmsg_RETURN index
comment     AD8C   .              ;

lcomment    AE28    disable BREAK
lcomment    AE2F    re-enable BREAK
label       AE32    _rts_ae32

comment     AE33

label       AEC0    _rts_aec0

const       AEC8

comment     AECE
comment     AECE   .   Print an error message, with current line number if one is active,
comment     AECE   .   and then return to the interpreter prompt.
comment     AECE   .   B is index of message from $01 = bmsg_CONT to $13 = bmsg_BreakInLine
label       AECE    perror
lcomment    AED2    B ≤ $13?
lcomment    AED4    yes, handle it here
lcomment    AED6    no, assume expansion set up jmp_perrorex
comment     AED9   .              ;
label       AED9    _per_findmsg
lcomment    AED9    load address of message 1
label       AEDC    _per_nextmsg
lcomment    AEDC    is this the message number?
lcomment    AEDD    yes, go print it
label       AEDF    _per_skipmsg
lcomment    AEDF    no, skip past this message
lcomment    AEE3    continue until end of message (MSB set)
lcomment    AEE5    message skipped; check next one

comment     AEE7   .              ;
label       AEE7    _per_printmsg
lcomment    AEE7    print the selected message
lcomment    AEE9    followed by " error"
lcomment    AEEE    is current program line 0?
lcomment    AEF0    yes, no current line, we're done
lcomment    AEF2    otherwise print " in"
lcomment    AEF7   .  and a newline
lcomment    AEFD    ??? progline+2
label       AF07    _jmp_BASIC_warmstart

comment     AF0A
label       AF0A    jmp_prstr8
lcomment    AF0A    trampoline
comment     AF0D
comment     AF0D   .   Reset to text mode, beep and newline.
label       AF0D    errmsg_setup
label       AF18    _jmp_prnl
lcomment    AF18    trampoline and often RTS

comment     AF1B
label       AF1B    breakkey

comment     AF26
comment     AF26   .   Print "Break in line" followed by
comment     AF26   .   ??? line number of line after progline
label       AF26    prbreak
comment     AF28   .              ;   First, see if we have a subsequent line that we now won't
comment     AF28   .              ;   execute. If not, the program was not interrupted.
lcomment    AF28    get current line
lcomment    AF2A    get following line
lcomment    AF2C    no following line: return
comment     AF2E   .              ;   Locations below $800 are not BASIC program text.
comment     AF2E   .              ;   (??? What does progline < $800 mean?)
lcomment    AF30    is address ≥$800?
lcomment    AF32    no, return
comment     AF34   .              ;
lcomment    AF39    current line data structure
lcomment    AF3B    skip past word 0
lcomment    AF3C    to line number word
lcomment    AF40    RTS

comment     AF42
comment     AF42   .   Ensure our next print call will print standard text,
comment     AF42   .   not an alternate charset or semi-graphics.
label       AF42    _textmode
lcomment    AF44    mask out bits 7 (semi-graphics) and 6 (alt charset)

comment     AF49
label       AF49    readyprompt
data        AF49-AF4F
comment     AF50
comment     AF50   .   Can't see where this is ever called from, but it appears
comment     AF50   .   to be code; most addrs are valid subroutines called elsewhere.
label       AF50    UNK_AF50

label       AF65    _af65
label       AF6C    _af6c
comment     AF72

comment     B021
comment     B032   .              ;
lcomment    B032    input buf ptr for entrymatch
lcomment    B038    entry list for entrymatch
lcomment    B03E    save input buf ptr
label       B04C    _b04c
label       B058    _b058
label       B061    _b061
label       B06D    _b06d
label       B072    _b072
label       B07A    _b07a
lcomment    B07A    ??? value at $E0 never used? (does not
lcomment    B07A   .  seem to be LSB of $DF, used by BIOS CMT)
label       B07D    _b07d
lcomment    B07D    LDAB #$0D and JMP BASIC_BFFA

comment     B080
comment     B080    ----------------------------------------------------------------------
comment     B080
comment     B080   .   Devices supported by this ROM. May be replaced by changing `devtab`.
label       B080    devtab_default
char        B080-B08A

comment     B08B
comment     B08B   .   XXX Maybe not all data, but nothing jumps directly into this block.
label       B08B    mB088
const       B08B-B0E1

comment     B0E2
comment     B0E2    ----------------------------------------------------------------------
comment     B0E2
lcomment    B0E2    bmsg_FileAccess index

comment     B119
const       B11A

comment     B19D
label       B1A9    _b1a9
label       B1B6    _b1b6
label       B1C4    _b1c4
label       B1D2    _b1d2
label       B1DF    _b1df
label       B1E1    _b1e1
label       B1F1    _b1f1
label       B1F3    _b1f3
label       B209    _b209
label       B212    _b212

label       B213    _bra_b19d
lcomment    B213    also a trampoline

comment     B215
label       B215    _jmp_b0e2
lcomment    B215    trampoline

comment     B218
label       B218    _b218
label       B220    _rts_b220

comment     B221

const       B281

comment     B300   .              ;
lcomment    B300    bmsg_Value index
comment     B305   .              ;

const       B671

lcomment    B73A    ??? RTS? But why apparent fallthrough?
label       B73D    _rts_b73d

comment     B741
lcomment    B74A    RTS
char        B747
lcomment    B746    space?
lcomment    B748    yes, go ???
comment     B74D   .              ;
label       B74D    _b74d

comment     B890
comment     B890   .   ♡B

comment     B89F
label       B8A9    _rts_b8a9

comment     B8AA

comment     B8ED
label       B8F7    _ldax_jmp_is10
label       B8F9    _jmp_is10digit
comment     B8FC   .              ; fallthough for is10digit JSR entry points
comment     B8FC
label       B909    _rts_b909

comment     B90A
label       B90A    perr_Syntax
lcomment    B90A    bmsg_Syntax index
label       B90E    perr_OutOfMem
lcomment    B90E    bmsg_OutOfMem index
label       B912    perr_UndefLineNo
lcomment    B912    bmsg_UndefLineNo index
label       B914    _b914

comment     B917
comment     B917   .   Keyword matching
comment     B917   .   Input: X pointing to current position in input buffer.
comment     B917   .   Returns:
comment     B917   .     ♠ A: ??? kw index? but somtimes modified.
comment     B917   .     ♠ B: ??? kw group? range 0 to 4
comment     B917   .     ♠ X: ??? after keyword on success, at start on failure
comment     B917   .     ♣ tmp3: ??? destroyed or maybe copy of input X
label       B917    kwmatch
lcomment    B917    setup for qrl_strmatch
lcomment    B91B    BASIC keywords table (1=NEW, ...)
label       B91E    entrymatch
lcomment    B91E    Entry point for different table (X).
lcomment    B91E    Caller must set rdlineptr{0,1} to input.
lcomment    B920    setup for qrl_strmatch
lcomment    B924    no match, go try extension
decimal     B927
lcomment    B928    if index >74 go ???
lcomment    B92B    ??? Knew through Kdim are immediate mode cmds?
decimal     B92C
lcomment    B930    ??? through Ktab or Kto
decimal     B931
comment     B935   .              ;   Enter here with B=0,1,2 per above
label       B935    _found_b935
lcomment    B935    ??? set MSBit, why?
comment     B938   .              ;
label       B938    _b938
decimal     B939
decimal     B93B
decimal     B93D
label       B941    _ret_b941
comment     B942   .              ;
label       B942    _tryext_b942
lcomment    B942    BASIC keyword extension installed?
lcomment    B945    no, go return notfound
lcomment    B947    RTS; let it have a try at matching
label       B949    _notfound_b949
lcomment    B949    RTS

comment     B94C

lcomment    BD2E    ptr to line (or end ptr after lines)
lcomment    BD30    lineno not found, return w/carry set
lcomment    BD34    load pointer to next line

comment     BC85
comment     BC85   .   ♠X ♣AB Search for the line number given in findline_target, starting at
comment     BC85   .   the beginning of the BASIC program or at the line to which X points.
comment     BC85   .   On success, return carry clear and X pointing to the found line.
comment     BC85   .   On failure, return carry set and X pointing to the first line past
comment     BC85   .   where findline_target should be (which may be the end of the program).
lcomment    BC85    start at beginning of BASIC program text
label       BC85    findline_start
label       BC87    findline
label       BC8D    _fl_next
label       BC8F    _fl_start
lcomment    BC8F    end of program?
lcomment    BC91    yes, set carry and return
lcomment    BC93    MSB of this line's line number
lcomment    BC99    LSB of this line's line number
label       BC9D    _fl_rts
label       BC9E    _fl_fail

comment     BCA0

comment     BCBA

label       BD03    _rts_bd03

comment     BD04
comment     BD04   .   If X is not less than himem, perr_OutOfMem, otherwise just return.
label       BD04    checkmem

comment     BD10
comment     BD10    ----------------------------------------------------------------------
comment     BD10   .   subroutine loaded into 0138 vector
comment     BD10
label       BD25    _bd25
label       BD28    _bd28

comment     BD2B
comment     BD2B    ----------------------------------------------------------------------
comment     BD2B

label       BD54    _bd54
label       BD5D    _rts_bd5d
label       BD5E    _bd5e

char        BD74-BF03
comment     BD74
comment     BD74    ----------------------------------------------------------------------
comment     BD74   .  Keywords
comment     BD74   .  These unused labels make the disassembler line things up nicely.
comment     BD74
label       BD74    Knew
label       BD77    Kmon
label       BD7A    Kcont
label       BD7E    Kfind
label       BD83    Klfind
label       BD89    Kdelete
label       BD90    Kauto
label       BD94    Klist
label       BD98    Kllist
label       BDA4    Ksave
label       BDA9    Kplay
label       BDAE    Kdim
label       BDB2    Kfor
label       BDB6    Kon
label       BDB8    Kif
label       BDBA    Kread
label       BDBF    Kpick
label       BDC4    Kinput
label       BDCA    Kopen
label       BDCF    Klet
label       BDD3    Kcolor
label       BDD9    Kplot
label       BDDE    Klocate
label       BDE5    Ksound
label       BDEB    Kpoke
label       BDF0    Kgoto
label       BDF5    Kgosub
label       BDFB    Kinput
label       BE02    Kprint
label       BE09    Ktempo
label       BE0F    Kclear
label       BE14    Ksave
label       BE19    Kload
label       BE1D    Kverify
label       BE23    Kmload
label       BE28    Krun
label       BE2B    Kbeep
label       BE2F    Knext
label       BE35    Kstore
label       BE3A    Kprint
label       BE3F    Klprint
label       BE45    Kclose
label       BE4A    Krem
label       BE4D    Kdata
label       BE51    Khcopy
label       BE56    Kcls
label       BE59    Kstop
label       BE5D    Kend
label       BE60    Kreturn
label       BE66    Krandomize
label       BE6F    Kinit
label       BE74    Kspc
label       BE78    Ktab
label       BE7C    Kto
label       BE7E    Kstep_
label       BE83    Kthen_
label       BE88    Keof
label       BE8C    Kne
label       BE8E    Kle
label       BE90    Kge
label       BE92    Klt
label       BE93    Keq
label       BE94    Kgt
label       BE95    Kplus
label       BE96    Kminus
label       BE97    Ktimes
label       BE98    Kdiv
label       BE99    Kpower
label       BE9A    Knot_
label       BE9E    Kand_
label       BEA2    Kor
label       BEA4    Kfre
label       BEA7    Khpos
label       BEAB    Kvpos
label       BEAF    Krnd
label       BEB2    Kpeek
label       BEB6    Kabs
label       BEB9    Kint
label       BEBC    Ksgn
label       BEBF    Ksqr
label       BEBF    Ksqr
label       BEC2    Ksin
label       BEC5    Kcos
label       BEC8    Kexp
label       BECB    Klog
label       BECE    Kasc
label       BED1    Klen
label       BED4    Kval
label       BED7    Kusr
label       BEDA    Kmod
label       BEDD    Kvar
label       BEE0    Kptr
label       BEE3    Kstick
label       BEE8    KchrS
label       BEEC    KhexS
label       BEF0    KstrS
label       BEF4    KleftS
label       BEF9    KrightS
label       BEFF    KmidS
label       BEFF    KmidS
label       BF03    K00

comment     BF04
comment     BF04   .   XXX not confirmed, but this looks too dodgy to be code.
data        BF04-BFF9
hex         BF04-BFF9   * Doesn't look like ASCII. But GRR, this directive doesn't work.
hex         BF0B        * Even explicitly marking one loc doesn't work
bin         BF0D        * Though binary for some reason does.

comment     BFFA
comment     BFFA   .   Standard entry points (used by BIOS and expansion code, too)


********************************************************************
*   BIOS routines used by BASIC
*
*   These are only the summary comments for the BASIC disassembly
*   file; see the BIOS disassembly file for details on the routines.

comment     E000
comment     E000   .   BIOS routines

lcomment    EB79    ♠X,screenptr0 ♡AB from curlin,curcol
lcomment    EB93    ♠X,tmp0 ♣B Return screen char [X] color attr addr
lcomment    EBE7    ♡ABX ♣tmp2 print character in A
comment     EEFE    ♣ABX print [X] as ASCII decimal, followed by a space
lcomment    EFA1    ♠A ♡BX upcase on character at X.
lcomment    EFA3    ♠A ♡BX If A is letter, upcase and C=0.
lcomment    EFA3   .       Otherwise C=1.
lcomment    EFF9    print MSBit-set-terminated string
lcomment    F006    ♠X print $00-terminated string
lcomment    F00F    print a newline
lcomment    F00F    works even when `pcolor` is graphics/alt charset
