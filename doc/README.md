JR-200 Documentation
====================

The following scans of hand-written files and schematics appear to be
English-language technical documentation from Matsushita System Engineering
Co., Ltd. The original source isn't clear, but they are hosted on
`hcvgm.org`; archive.org's copy of the original [index page][hc index]
gives URLs that are not archived but still work on the original site.

- [Schematic Info.](jr200_Schematic.pdf) ([original][hc sch])
- [JR-200 IO Interface Specification (Hardware)
  ](jr200_IO_Interface_Spec.pdf) ([original][hc io])
- [JR-200 and RS-232C Interface](jr200_and_rs232c_Interface.pdf)
  ([original][hc rs232])
- [Operating System Routines](jr200_OS_Routines.pdf) ([original][hc os])

JR-100 ROM thing:
<https://archive.org/details/io-198205/page/n4/mode/1up?view=theater>



<!-------------------------------------------------------------------->
[hc index]: https://web.archive.org/web/20160324131954/http://hcvgm.org/Static/Manuals/JR-200/
[hc io]: http://hcvgm.org/Static/Manuals/JR-200/jr200_IO_Interface_Spec.pdf
[hc os]: http://hcvgm.org/Static/Manuals/JR-200/jr200_OS_Routines.pdf
[hc rs232]: http://hcvgm.org/Static/Manuals/JR-200/jr200_and_rs232c_Interface.pdf
[hc sch]: http://hcvgm.org/Static/Manuals/JR-200/jr200_Schematic.pdf
