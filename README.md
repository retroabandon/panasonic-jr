National/Panasonic JR-series Images
===================================

JR-100
------

Nothing here yet.


JR-200
------

References:
- cjs's [sedoc JR-200][sed jr200] information.
- Markku Reunanen, [Discovering the Panasonic JR-200U][marq]

Disassembly-related files (see next section for more information):
- `*.info`: Disassembly configuration files.
- `*.dis`: Disassembly output.
- `*.r2`: Radare2 configuration files for corresponding binary files.
  See below for more information.

ROM dumps (see [sedoc JR-200 ROM][sed rom] for version information):
- `An-BASIC/`: Dumps of the BASIC ROM, IC5 in the centre of the PCB. These
  are marked `A1` for v5.00 and `A2` for v5.01.
- `Bn-BIOS/`: Dumps of the BIOS ROM, IC6 towards the front of the PCB. The
  are marked `B1` on v5.00 units and `B2` on v5.01 units.

Other files:
- [`marq/sysrom.lst`](marq/sysrom.lst),
  [`marq/basic.lst`](marq/basic.lst):
  Raw DASMx dump/disassembly runs from [Markku Reunanen][marq] of
  system ([source][marq-sys]) and BASIC ([source][marq-bas]) ROMs from
  a JR-200U (European, perhaps Finnish version). Aside from
  auto-generated labels, these include no further information, not
  even vector identification or data/code separation.


Disassembly System
------------------

This project uses [f9dasm] for disassembly, but our default submodule
configuration uses [cjs's fork][f9dasm-cjs] of it to include useful
patches not in the upstream disassembler. The build system is
substantially similar to the [retroabandon/fm7re] project. Run `make`
to run the disassembly.

The disassemblies, `*.dis` files, are committed to the repo for easy
viewing through web interfaces to the repo. These are generated from
`*.bin` (binary code) and `*.info` (annotation) files, with
`common.info` holding common information used in multiple
disassemblies.

The documentation for the `.info` file syntax can be viewed on-line
[here][f9dasm-doc].

Please ensure that if you set options for more debugging information in the
disassembly output (usually the ones in the top-level `common.info`) you
disable them again and re-run the disassembly before commiting the new
disassembly output.

#### Radare2

[Radare2] is a good tool for interactive examination of binary files,
though it's not terribly useful for serious disassembly. We leave here
Radare2 configuration files for certain binaries in case anybody wants
to try to take this tool further.



<!-------------------------------------------------------------------->
[marq-bas]: http://www.kameli.net/~marq/jr200/basic.lst
[marq-sys]: http://www.kameli.net/~marq/jr200/sysrom.lst
[marq]: http://www.kameli.net/marq/?page_id=1270
[sed jr200]: https://github.com/0cjs/sedoc/blob/master/8bit/jr200.md
[sed rom]: https://github.com/0cjs/sedoc/blob/master/8bit/jr-200/rom.md

[f9dasm-cjs]: https://github.com/0cjs/f9dasm
[f9dasm-doc]: https://htmlpreview.github.io/?https://github.com/Arakula/f9dasm/blob/master/f9dasm.htm
[f9dasm]: https://github.com/Arakula/f9dasm
[radare2]: https://github.com/0cjs/sedoc/tree/master/app/radare2.md
[retroabandon/fm7re]: https://gitlab.com/retroabandon/fm7re
[retroabandon/hitachi-basic-master-re]: https://gitlab.com/retroabandon/hitachi-basic-master-re
[retroabandon/panasonic-jr]: https://gitlab.com/retroabandon/panasonic-jr
