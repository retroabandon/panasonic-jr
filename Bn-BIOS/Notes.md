BIOS ROM Disassembly Notes
==========================


Version Differences
-------------------

The B1 and B2 versions have very few differences:

    Address     B1          B2
    E610        4F97        7F00
    E6F6-E731
    EA33-EA48
    F065        D4          D0
