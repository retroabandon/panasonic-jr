JR-100 Files
===========

There is a single 8K ROM mapped at $E000 to $FFFF.

- `JR-100U.bin`: JR-100U (European version) ROM dump from [cj7hawk on
  forum.vcfed.org][cj7hawk]. In that message the file is named
  `david-JR-100U.BIN`.



<!-------------------------------------------------------------------->
[cj7hawk]: https://forum.vcfed.org/index.php?threads/kaypro-damage-by-local-government-is-it-just-an-australian-thing.1246542/post-1363103
